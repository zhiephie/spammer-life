'use strict';
import React, {
  Component,
} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  View
} from 'react-native';
import Button from 'react-native-button';
import {Actions} from 'react-native-router-flux';
import { Artists } from '../../mockData';
import ArtistListItem from './ArtistListItem';

import { 
  AdMobBanner
} from 'react-native-admob';

class ArtistList extends Component {
  constructor(props){
    super(props);
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows( Artists ),
    }
  }

  fetchData() {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson.movies;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Artists
        </Text>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={ ( artist ) => <ArtistListItem artist={ artist } /> }/>
          <View>
              <AdMobBanner
                  bannerSize="smartBannerLandscape"
                  adUnitID="ca-app-pub-5629971706269941/7402335512"
                  testDeviceID="EMULATOR"
                  didFailToReceiveAdWithError={this.bannerError}
              />
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#111',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#fff',
  },
  instructions: {
    textAlign: 'center',
    color: '#888',
    marginBottom: 5,
  },
});

module.exports = ArtistList;
