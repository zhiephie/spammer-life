const Artists = [
  {
    name: "Via Valen",
    background: "http://www.intrawallpaper.com/static/images/nature-hd-background-4_LNtWeA7.jpg",
    songs: [
      {
        title: "Dia",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/brisxg7v0eap3r5/Via_Vallen_Dia_Sera_Klaten_2017.mp3",
      },
      {
        title: "Aku Lelakimu",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/ypog9vkcqpewoe7/Via%20Vallen%20-%20Aku%20Lelakimu%20-%20OM.Sera%20Live%20Taman%20Wisata%20Tawun%20Ngawi%202016.mp3",
      },
      {
        title: "Lungset",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/wo6zv7cdffzmx02/VIA%20VALLEN%20%20-%20%20LUNGSET.mp3",
      },
      {
        title: "Selimut Tetangga",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/zz6qalwbolrek5m/Via%20Vallen%20-%20Selimut%20Tetangga.mp3",
      },
      {
        title: "Selingkuh",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/y5awadoalkenmc3/Via%20Vallen%20%20Selingkuh%20%20-%20Seleb%20On%20News%20Awards%202016.mp3",
      },
      {
        title: "Berkali Kali",
        album: "Via Valen",
        // albumImage: "http://cdn1-a.production.liputan6.static6.com/medias/1015351/big-portrait/017582000_1444374763-fia_valen.jpg",
        url: "https://dl.dropboxusercontent.com/s/sh7yfj2fc2lai1h/Disco%20dangdut%20remix-Via%20vallen%20Berkali%20kali%20By%20agecirata87%40gmail.com.mp3",
      }
    ]
  },
  {
    name: "Rena KDI",
    background: "http://www.intrawallpaper.com/static/images/Hd-Backgrounds-371.jpg",
    songs: [
      {
        title: "Oleh Oleh",
        album: "Rena KDI",
        // albumImage: "https://1.bp.blogspot.com/-m9IjOPDfLGw/VzvD_fXK4ZI/AAAAAAAADME/S5F2ArSBCLw5XsrupkGR_CDAuJC2v9x6wCLcB/s1600/phoca_thumb_l_rena%2Bkdi%2B14.jpg",
        url: "https://dl.dropboxusercontent.com/s/tpfu3mvtww6hxs6/Rena%20KDI%20-%20Oleh%20Oleh%2C%20Netral%20%28PDSI%29%20Monata%202014-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Kelangan",
        album: "Rena KDI",
        // albumImage: "https://1.bp.blogspot.com/-m9IjOPDfLGw/VzvD_fXK4ZI/AAAAAAAADME/S5F2ArSBCLw5XsrupkGR_CDAuJC2v9x6wCLcB/s1600/phoca_thumb_l_rena%2Bkdi%2B14.jpg",
        url: "https://dl.dropboxusercontent.com/s/vk1imiunmuw7rzm/KELANGAN%20%20%20RENA%20KDI%20MONATA%20FEAT%20RYU%20STAR-%5Bwww.stafabanddl.info%5D.mp3",
      }
    ]
  },
  {
    name: "Lilin Herlina",
    background: "http://www.intrawallpaper.com/static/images/nature-hd-background-4_LNtWeA7.jpg",
    songs: [
      {
        title: "Suket Teki",
        album: "Lilin Herlina",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/artist/avatar/8/thumb_8.jpg",
        url: "https://dl.dropboxusercontent.com/s/12kamqigum9mf96/SUKET%20TEKI%20-%20LILIN%20HERLINA%20%20-%20NEW%20PALLAPA%20LIVE%20PT.KAS%20SNP%20KM24%20MANYAR%20GRESIK%20-%20MAXTONES%20PRO-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Oleh Oleh",
        album: "Lilin Herlina",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/artist/avatar/8/thumb_8.jpg",
        url: "https://dl.dropboxusercontent.com/s/s13c3ysf5jcl1hk/Lilin%20Herlina%20-%20Oleh%20Oleh%20%28Bintang%20Pantura%203%29-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Haruskah Berakhir",
        album: "Lilin Herlina",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/artist/avatar/8/thumb_8.jpg",
        url: "https://dl.dropboxusercontent.com/s/7gkka9kdshmkyx5/Haruskah%20Berakhir%20Lilin%20Herlina.mpg-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Tabir Kepalsuan",
        album: "Lilin Herlina",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/artist/avatar/8/thumb_8.jpg",
        url: "https://dl.dropboxusercontent.com/s/c4in5wj9i8nuh4e/Tabir%20Kepalsuan-Monata2011.%20lilin%20herlina-%5Bwww.stafabanddl.info%5D.mp3",
      }
    ]
  },
  {
    name: "Eny Sagita",
    background: "http://www.intrawallpaper.com/static/images/nature-hd-background-25.jpg",
    songs: [
      {
        title: "Ngamen 17",
        album: "Eny Sagita",
        // albumImage: "http://2.bp.blogspot.com/-i3dNM_16F-I/VYzpwzfjdeI/AAAAAAAAAQU/eE3jfkOTz4c/s1600/eny%2Bsagita.jpeg",
        url: "https://dl.dropboxusercontent.com/s/6gg9vflh6xe3ywz/Eny%20Sagita%20-%20Ngamen%2017.mp3",
      },
      {
        title: "Kereta Malam",
        album: "Eny Sagita",
        // albumImage: "http://2.bp.blogspot.com/-i3dNM_16F-I/VYzpwzfjdeI/AAAAAAAAAQU/eE3jfkOTz4c/s1600/eny%2Bsagita.jpeg",
        url: "https://dl.dropboxusercontent.com/s/dbstewbo4565dd6/KERETA%20MALAM%20-%20ENY%20SAGITA.mp3",
      },
      {
        title: "Kapokmu Kapan",
        album: "Eny Sagita",
        // albumImage: "http://2.bp.blogspot.com/-i3dNM_16F-I/VYzpwzfjdeI/AAAAAAAAAQU/eE3jfkOTz4c/s1600/eny%2Bsagita.jpeg",
        url: "https://dl.dropboxusercontent.com/s/3e9deb5g43b5sxm/ENY%20SAGITA%20-%20KAPOKMU%20KAPAN%20%28MUSIK%20SAGITA%20ALBUM%20NGAMEN%2017%29.mp3",
      }
    ]
  },
  {
    name: "Anjar Agustine",
    background: "http://www.intrawallpaper.com/static/images/Hd-Backgrounds-371.jpg",
    songs: [
      {
        title: "Kandas",
        album: "Anjar Agustine",
        // albumImage: "https://1.bp.blogspot.com/-f0KlA7tyqgs/WDusDhL9k3I/AAAAAAAAHu8/FfQunPJxaEElFmNSu7cxgdwczivwTnMuACLcB/s1600/Anjar%2BAgustin%2BCantik.jpg",
        url: "https://dl.dropboxusercontent.com/s/trvdr0p2ybgdugd/%27%27KANDAS%27%27%20Anjar%20Agustine%20Feat%20Sodiq-%20MONATA%20-%20YouTube2.flv-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Cinta Abadi",
        album: "Anjar Agustine",
        // albumImage: "https://1.bp.blogspot.com/-f0KlA7tyqgs/WDusDhL9k3I/AAAAAAAAHu8/FfQunPJxaEElFmNSu7cxgdwczivwTnMuACLcB/s1600/Anjar%2BAgustin%2BCantik.jpg",
        url: "https://dl.dropboxusercontent.com/s/p0mhzkcbu1fmbge/%27%27Monata2012%20-%20CINTA%20ABADI%20%20ANJAR%20AGUSTINE%20Feat%20SHODIQ-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Secawang Madu",
        album: "Anjar Agustine",
        // albumImage: "https://1.bp.blogspot.com/-f0KlA7tyqgs/WDusDhL9k3I/AAAAAAAAHu8/FfQunPJxaEElFmNSu7cxgdwczivwTnMuACLcB/s1600/Anjar%2BAgustin%2BCantik.jpg",
        url: "https://dl.dropboxusercontent.com/s/9yuutm01l9jen3o/SECAWAN%20MADU%20-%20ANJAR%20AGUSTIN%20-%20MONATA%20LIVE%20YOGA%20.mp3",
      }
    ]
  },
  {
    name: "Andra Kharisma",
    background: "http://www.intrawallpaper.com/static/images/nature-hd-background-25.jpg",
    songs: [
      {
        title: "Istimewa",
        album: "Andra Kharisma",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/photo/content/176112/trim_14099300_1191268260937826_60074006_n.jpg",
        url: "https://dl.dropboxusercontent.com/s/ba9d765ezkn8x6w/ISTIMEWA%20ANDRA%20KHARISMA%20LIVE%20BLITAR-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Ra Kuat Mbok",
        album: "Andra Kharisma",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/photo/content/176112/trim_14099300_1191268260937826_60074006_n.jpg",
        url: "https://dl.dropboxusercontent.com/s/19d8mukz4goxb8z/Ra%20Kuat%20Mbok-Andra%20Kharisma-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Salah Tompo",
        album: "Andra Kharisma",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/photo/content/176112/trim_14099300_1191268260937826_60074006_n.jpg",
        url: "https://dl.dropboxusercontent.com/s/pu59yf0bqbcp9dm/Salah%20Tompo%20%20Andra%20Kharisma%20OM%20New%20Scorpio%20Dangdut%20Terbaru%20Oktober%202016-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Sunset Di Tanah Anarki",
        album: "Andra Kharisma",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/photo/content/176112/trim_14099300_1191268260937826_60074006_n.jpg",
        url: "https://dl.dropboxusercontent.com/s/qx5ty0zbdzwdkz5/SUNSET%20DI%20TANAH%20ANARKI-ANDRA%20KHARISMA-OM.PUTRA%20GRG%20JOMBANG-%5Bwww.stafabanddl.info%5D.mp3",
      },
      {
        title: "Tembang Tresno",
        album: "Andra Kharisma",
        // albumImage: "http://cdn.bintangdangdut.net/bintangdangdut/uploads/photo/content/176112/trim_14099300_1191268260937826_60074006_n.jpg",
        url: "https://dl.dropboxusercontent.com/s/jkj7sctuhs0o18n/TEMBANG%20TRESNO%20Andra%20Kharisma%20NEW%20KENDEDES%20%28%20Serga%20Chanel%20%29-%5Bwww.stafabanddl.info%5D.mp3",
      }
    ]
  }
]

module.exports = { Artists: Artists };
